<?php

namespace App\Http\Livewire;

use App\Models\Terceros;
use Livewire\Component;
use App\Models\AgendarCitas;

class FormCitas extends Component
{

    public $paciente, $doctor, $datetimes, $observacion;

    public function create()
    {
        $messages = [
            'paciente.required' => 'El campo paciente es obligatorio',
            'medico.required' => 'El campo medico es obligatorio',
            'datetimes.required' => 'El campo fecha es obligatorio',
            'observacion.required' => 'El campo observacion es obligatorio'

        ];
        $this->validate([
            'paciente' => 'required',
            'doctor' => 'integer',
            'datetimes' => 'required',
            'observacion' => 'required'
        ], $messages);

        AgendarCitas::create([
            'id_tercero_paciente' => $this->paciente,
            'id_tercero_medico' => $this->doctor,
            'fecha_consulta' => $this->datetimes,
            'observacion' => $this->observacion
        ]);

        $this->reset(['paciente', 'doctor', 'datetimes', 'observacion']);
    }

    public function render()
    {
        $medicos = terceros::get()->where('typeTercero', '=', 'Médico');
        $clientes = terceros::get()->where('typeTercero', '=', 'Cliente');
        $citas = AgendarCitas::all();

        return view('livewire.form-citas', compact('medicos', 'clientes', 'citas'));
    }
}
