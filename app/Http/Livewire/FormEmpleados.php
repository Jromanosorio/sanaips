<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Empleados;


class FormEmpleados extends Component
{
    public $viewButton = 'store';
    public $cEmpleado, $name, $cargo;
    public function render()
    {
        return view('livewire.form-empleados' , [
            'empleados' => Empleados::paginate(5)
        ]);
    }
    public function store(){
        $messages = [
            'name.required' => 'El campo nombre es obligatorio ',
            'cargo.required' => 'El campo cargo es obligatorio',
            'cEmpleado.required' => 'El campo código es obligatorio',
        ];
        $this->validate([
            'name' => 'required',
            'cEmpleado' => 'required',
            'cargo' => 'required',
        ], $messages);

        Empleados::create([
            'name' => $this->name,
            'cargo' => $this->cargo,
            'cEmpleado' => $this->cEmpleado,

        ]);
        $this->reset(['name', 'cEmpleado', 'cargo']);

        $this->alert('success', 'Empleado creado correctamente!', [
            'position' =>  'top-end', 
            'timer' =>  '4000', 
            'toast' =>  true, 
            'text' =>  '', 
            'confirmButtonText' =>  'Ok', 
            'cancelButtonText' =>  'Cancel', 
            'showCancelButton' =>  false, 
            'showConfirmButton' =>  false, 
        ]);
    }
}
