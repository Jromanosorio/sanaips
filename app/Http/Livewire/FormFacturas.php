<?php

namespace App\Http\Livewire;

use App\Models\Facturas;
use Livewire\Component;
use Livewire\WithPagination;

class FormFacturas extends Component
{
    
    public $consecutivo = 00001;
    public $nFactura = '00001';
    public $centroCostosName = 'Rubro';
    public $plazoName = 'X meses';

    public $view = 'store';
    public $viewButton = 'store';
    public $buscar, $fechaIngreso, $prefijo, $plazo, $centroCostos,
        $idVendedor, $productoServicio, $descuento, $nameTercero, $adress, $city, $phone, $rLegal, $nameTerceroVendedor;

    public function doSomething(){
        $this->alert('success', 'calculando Iva!', [
            'position' =>  'top-end', 
            'timer' =>  '4000', 
            'toast' =>  true, 
            'text' =>  '', 
            'confirmButtonText' =>  'Ok', 
            'cancelButtonText' =>  'Cancel', 
            'showCancelButton' =>  false, 
            'showConfirmButton' =>  false, 
        ]);
    }
    public function render()
    {
        return view('livewire.form-facturas', [
            'facturas' => Facturas::paginate(3),
            
        ]);
    }
    
    public function changeView($data){        
        $this->view = $data;

    }
}
