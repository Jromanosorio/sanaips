<?php

namespace App\Http\Livewire;
use App\Models\Terceros;
use Livewire\Component;
use Livewire\WithPagination;

class FormTerceros extends Component
{
    public $locations = 
    ['Abejorral - Antioquia',
    'Abriaquí - Antioquia',
    'Alejandría - Antioquia',
    'Amagá - Antioquia',
    'Amalfi - Antioquia',
    'Andes - Antioquia',
    'Angelópolis - Antioquia',
    'Angostura - Antioquia',
    'Anorí - Antioquia',
    'Anzá - Antioquia',
    'Apartadó - Antioquia',
    'Arboletes - Antioquia',
    'Argelia - Antioquia',
    'Armenia - Antioquia',
    'Barbosa - Antioquia',
    'Bello - Antioquia',
    'Belmira - Antioquia',
    'Betania - Antioquia',
    'Betulia - Antioquia',
    'Briceño - Antioquia',
    'Buriticá - Antioquia',
    'Cáceres - Antioquia',
    'Caicedo - Antioquia',
    'Caldas - Antioquia',
    'Campamento - Antioquia',
    'Cañasgordas - Antioquia',
    'Caracolí - Antioquia',
    'Caramanta - Antioquia',
    'Carepa - Antioquia',
    'Carolina - Antioquia',
    'Caucasia - Antioquia',
    'Chigorodó - Antioquia',
    'Cisneros - Antioquia',
    'Ciudad Bolívar - Antioquia',
    'Cocorná - Antioquia',
    'Concepción - Antioquia',
    'Concordia - Antioquia',
    'Copacabana - Antioquia',
    'Dabeiba - Antioquia',
    'Donmatías - Antioquia',
    'Ebéjico - Antioquia',
    'El Bagre - Antioquia',
    'El Carmen De Viboral - Antioquia',
    'El Santuario - Antioquia',
    'Entrerríos - Antioquia',
    'Envigado - Antioquia',
    'Fredonia - Antioquia',
    'Frontino - Antioquia',
    'Giraldo - Antioquia',
    'Girardota - Antioquia',
    'Gómez Plata - Antioquia',
    'Granada - Antioquia',
    'Guadalupe - Antioquia',
    'Guarne - Antioquia',
    'Guatapé - Antioquia',
    'Heliconia - Antioquia',
    'Hispania - Antioquia',
    'Itagüí - Antioquia',
    'Ituango - Antioquia',
    'Jardín - Antioquia',
    'Jericó - Antioquia',
    'La Ceja - Antioquia',
    'La Estrella - Antioquia',
    'La Pintada - Antioquia',
    'La Unión - Antioquia',
    'Liborina - Antioquia',
    'Maceo - Antioquia',
    'Marinilla - Antioquia',
    'Medellín - Antioquia',
    'Montebello - Antioquia',
    'Murindó - Antioquia',
    'Mutatá - Antioquia',
    'Nariño - Antioquia',
    'Nechí - Antioquia',
    'Necoclí - Antioquia',
    'Olaya - Antioquia',
    'Peñol - Antioquia',
    'Peque - Antioquia',
    'Pueblorrico - Antioquia',
    'Puerto Berrío - Antioquia',
    'Puerto Nare - Antioquia',
    'Puerto Triunfo - Antioquia',
    'Remedios - Antioquia',
    'Retiro - Antioquia',
    'Rionegro - Antioquia',
    'Sabanalarga - Antioquia',
    'Sabaneta - Antioquia',
    'Salgar - Antioquia',
    'San Andrés De Cuerquía - Antioquia',
    'San Carlos - Antioquia',
    'San Francisco - Antioquia',
    'San Jerónimo - Antioquia',
    'San José De La Montaña - Antioquia',
    'San Juan De Urabá - Antioquia',
    'San Luis - Antioquia',
    'San Pedro De Los Milagros - Antioquia',
    'San Pedro De Urabá - Antioquia',
    'San Rafael - Antioquia',
    'San Roque - Antioquia',
    'San Vicente Ferrer - Antioquia',
    'Santa Bárbara - Antioquia',
    'Santa Fé De Antioquia - Antioquia',
    'Santa Rosa De Osos - Antioquia',
    'Santo Domingo - Antioquia',
    'Segovia - Antioquia',
    'Sonsón - Antioquia',
    'Sopetrán - Antioquia',
    'Támesis - Antioquia',
    'Tarazá - Antioquia',
    'Tarso - Antioquia',
    'Titiribí - Antioquia',
    'Toledo - Antioquia',
    'Turbo - Antioquia',
    'Uramita - Antioquia',
    'Urrao - Antioquia',
    'Valdivia - Antioquia',
    'Valparaíso - Antioquia',
    'Vegachí - Antioquia',
    'Venecia - Antioquia',
    'Vigía Del Fuerte - Antioquia',
    'Yalí - Antioquia',
    'Yarumal - Antioquia',
    'Yolombó - Antioquia',
    'Yondó - Antioquia',
    'Zaragoza - Antioquia',
    'El Encanto - Amazonas',
    'La Chorrera - Amazonas',
    'La Pedrera - Amazonas',
    'La Victoria - Amazonas',
    'Leticia - Amazonas',
    'Mirití - Paraná - Amazonas',
    'Puerto Alegría - Amazonas',
    'Puerto Arica - Amazonas',
    'Puerto Nariño - Amazonas',
    'Puerto Santander - Amazonas',
    'Tarapacá - Amazonas',
    'Arauca - Arauca',
    'Arauquita - Arauca',
    'Cravo Norte - Arauca',
    'Fortul - Arauca',
    'Puerto Rondón - Arauca',
    'Saravena - Arauca',
    'Tame - Arauca',
    'Baranoa - Atlántico',
    'Barranquilla - Atlántico',
    'Campo De La Cruz - Atlántico',
    'Candelaria - Atlántico',
    'Galapa - Atlántico',
    'Juan De Acosta - Atlántico',
    'Luruaco - Atlántico',
    'Malambo - Atlántico',
    'Manatí - Atlántico',
    'Palmar De Varela - Atlántico',
    'Piojó - Atlántico',
    'Polonuevo - Atlántico',
    'Ponedera - Atlántico',
    'Puerto Colombia - Atlántico',
    'Repelón - Atlántico',
    'Sabanagrande - Atlántico',
    'Sabanalarga - Atlántico',
    'Santa Lucía - Atlántico',
    'Santo Tomás - Atlántico',
    'Soledad - Atlántico',
    'Suan - Atlántico',
    'Tubará - Atlántico',
    'Usiacurí - Atlántico',
    'Bogotá, D.C. - Bogotá, D. C.',
    'Achí - Bolívar',
    'Altos Del Rosario - Bolívar',
    'Arenal - Bolívar',
    'Arjona - Bolívar',
    'Arroyohondo - Bolívar',
    'Barranco De Loba - Bolívar',
    'Calamar - Bolívar',
    'Cantagallo - Bolívar',
    'Cartagena De Indias - Bolívar',
    'Cicuco - Bolívar',
    'Clemencia - Bolívar',
    'Córdoba - Bolívar',
    'El Carmen De Bolívar - Bolívar',
    'El Guamo - Bolívar',
    'El Peñón - Bolívar',
    'Hatillo De Loba - Bolívar',
    'Magangué - Bolívar',
    'Mahates - Bolívar',
    'Margarita - Bolívar',
    'María La Baja - Bolívar',
    'Mompós - Bolívar',
    'Montecristo - Bolívar',
    'Morales - Bolívar',
    'Norosí - Bolívar',
    'Pinillos - Bolívar',
    'Regidor - Bolívar',
    'Río Viejo - Bolívar',
    'San Cristóbal - Bolívar',
    'San Estanislao - Bolívar',
    'San Fernando - Bolívar',
    'San Jacinto - Bolívar',
    'San Jacinto Del Cauca - Bolívar',
    'San Juan Nepomuceno - Bolívar',
    'San Martín De Loba - Bolívar',
    'San Pablo - Bolívar',
    'Santa Catalina - Bolívar',
    'Santa Rosa - Bolívar',
    'Santa Rosa Del Sur - Bolívar',
    'Simití - Bolívar',
    'Soplaviento - Bolívar',
    'Talaigua Nuevo - Bolívar',
    'Tiquisio - Bolívar',
    'Turbaco - Bolívar',
    'Turbaná - Bolívar',
    'Villanueva - Bolívar',
    'Zambrano - Bolívar',
    'Almeida - Boyacá',
    'Aquitania - Boyacá',
    'Arcabuco - Boyacá',
    'Belén - Boyacá',
    'Berbeo - Boyacá',
    'Betéitiva - Boyacá',
    'Boavita - Boyacá',
    'Boyacá - Boyacá',
    'Briceño - Boyacá',
    'Buenavista - Boyacá',
    'Busbanzá - Boyacá',
    'Caldas - Boyacá',
    'Campohermoso - Boyacá',
    'Cerinza - Boyacá',
    'Chinavita - Boyacá',
    'Chiquinquirá - Boyacá',
    'Chíquiza - Boyacá',
    'Chiscas - Boyacá',
    'Chita - Boyacá',
    'Chitaraque - Boyacá',
    'Chivatá - Boyacá',
    'Chivor - Boyacá',
    'Ciénega - Boyacá',
    'Cómbita - Boyacá',
    'Coper - Boyacá',
    'Corrales - Boyacá',
    'Covarachía - Boyacá',
    'Cubará - Boyacá',
    'Cucaita - Boyacá',
    'Cuítiva - Boyacá',
    'Duitama - Boyacá',
    'El Cocuy - Boyacá',
    'El Espino - Boyacá',
    'Firavitoba - Boyacá',
    'Floresta - Boyacá',
    'Gachantivá - Boyacá',
    'Gámeza - Boyacá',
    'Garagoa - Boyacá',
    'Guacamayas - Boyacá',
    'Guateque - Boyacá',
    'Guayatá - Boyacá',
    'Güicán De La Sierra - Boyacá',
    'Iza - Boyacá',
    'Jenesano - Boyacá',
    'Jericó - Boyacá',
    'La Capilla - Boyacá',
    'La Uvita - Boyacá',
    'La Victoria - Boyacá',
    'Labranzagrande - Boyacá',
    'Macanal - Boyacá',
    'Maripí - Boyacá',
    'Miraflores - Boyacá',
    'Mongua - Boyacá',
    'Monguí - Boyacá',
    'Moniquirá - Boyacá',
    'Motavita - Boyacá',
    'Muzo - Boyacá',
    'Nobsa - Boyacá',
    'Nuevo Colón - Boyacá',
    'Oicatá - Boyacá',
    'Otanche - Boyacá',
    'Pachavita - Boyacá',
    'Páez - Boyacá',
    'Paipa - Boyacá',
    'Pajarito - Boyacá',
    'Panqueba - Boyacá',
    'Pauna - Boyacá',
    'Paya - Boyacá',
    'Paz De Río - Boyacá',
    'Pesca - Boyacá',
    'Pisba - Boyacá',
    'Puerto Boyacá - Boyacá',
    'Quípama - Boyacá',
    'Ramiriquí - Boyacá',
    'Ráquira - Boyacá',
    'Rondón - Boyacá',
    'Saboyá - Boyacá',
    'Sáchica - Boyacá',
    'Samacá - Boyacá',
    'San Eduardo - Boyacá',
    'San José De Pare - Boyacá',
    'San Luis De Gaceno - Boyacá',
    'San Mateo - Boyacá',
    'San Miguel De Sema - Boyacá',
    'San Pablo De Borbur - Boyacá',
    'Santa María - Boyacá',
    'Santa Rosa De Viterbo - Boyacá',
    'Santa Sofía - Boyacá',
    'Santana - Boyacá',
    'Sativanorte - Boyacá',
    'Sativasur - Boyacá',
    'Siachoque - Boyacá',
    'Soatá - Boyacá',
    'Socha - Boyacá',
    'Socotá - Boyacá',
    'Sogamoso - Boyacá',
    'Somondoco - Boyacá',
    'Sora - Boyacá',
    'Soracá - Boyacá',
    'Sotaquirá - Boyacá',
    'Susacón - Boyacá',
    'Sutamarchán - Boyacá',
    'Sutatenza - Boyacá',
    'Tasco - Boyacá',
    'Tenza - Boyacá',
    'Tibaná - Boyacá',
    'Tibasosa - Boyacá',
    'Tinjacá - Boyacá',
    'Tipacoque - Boyacá',
    'Toca - Boyacá',
    'Togüí - Boyacá',
    'Tópaga - Boyacá',
    'Tota - Boyacá',
    'Tunja - Boyacá',
    'Tununguá - Boyacá',
    'Turmequé - Boyacá',
    'Tuta - Boyacá',
    'Tutazá - Boyacá',
    'Úmbita - Boyacá',
    'Ventaquemada - Boyacá',
    'Villa De Leyva - Boyacá',
    'Viracachá - Boyacá',
    'Zetaquira - Boyacá',
    'Aguadas - Caldas',
    'Anserma - Caldas',
    'Aranzazu - Caldas',
    'Belalcázar - Caldas',
    'Chinchiná - Caldas',
    'Filadelfia - Caldas',
    'La Dorada - Caldas',
    'La Merced - Caldas',
    'Manizales - Caldas',
    'Manzanares - Caldas',
    'Marmato - Caldas',
    'Marquetalia - Caldas',
    'Marulanda - Caldas',
    'Neira - Caldas',
    'Norcasia - Caldas',
    'Pácora - Caldas',
    'Palestina - Caldas',
    'Pensilvania - Caldas',
    'Riosucio - Caldas',
    'Risaralda - Caldas',
    'Salamina - Caldas',
    'Samaná - Caldas',
    'San José - Caldas',
    'Supía - Caldas',
    'Victoria - Caldas',
    'Villamaría - Caldas',
    'Viterbo - Caldas',
    'Aguazul - Casanare',
    'Chámeza - Casanare',
    'Hato Corozal - Casanare',
    'La Salina - Casanare',
    'Maní - Casanare',
    'Monterrey - Casanare',
    'Nunchía - Casanare',
    'Orocué - Casanare',
    'Paz De Ariporo - Casanare',
    'Pore - Casanare',
    'Recetor - Casanare',
    'Sabanalarga - Casanare',
    'Sácama - Casanare',
    'San Luis De Palenque - Casanare',
    'Támara - Casanare',
    'Tauramena - Casanare',
    'Trinidad - Casanare',
    'Villanueva - Casanare',
    'Yopal - Casanare',
    ];
    public $typeIdentification, $identification,$dv, $secondName, 
        $firstName, $firtsLastName, $razonSocial, $secondLastName, 
        $address, $city, $typeTercero, $phone, $rLegal, $acompañante,
        $rFuente, $rIVA, $rIndustraComercio, $buscar,$terceroId;
    public $view = "show";
    public $viewButton = "store";
    protected $listeners = [
        'confirmed',
        'cancelled',        
    ];
    use WithPagination;
    public function render()
    {   
        return view('livewire.form-terceros', [
            'terceros' => Terceros::where('typeIdentification', 'LIKE', "%{$this->buscar}%")
                ->orWhere('identification', 'LIKE', "%{$this->buscar}%")
                ->orWhere('firstName', 'LIKE', "%{$this->buscar}%")
                ->orWhere('secondName', 'LIKE', "%{$this->buscar}%")
                ->orWhere('firtsLastName', 'LIKE', "%{$this->buscar}%")
                ->orWhere('secondLastName', 'LIKE', "%{$this->buscar}%")
                ->orWhere('razonSocial', 'LIKE', "%{$this->buscar}%")
                ->orWhere('address', 'LIKE', "%{$this->buscar}%")
                ->orWhere('typeTercero', 'LIKE', "%{$this->buscar}%")
                ->orWhere('city', 'LIKE', "%{$this->buscar}%")
                ->orderBy('id','DESC')
                ->paginate(7),
            'locations' => $this->locations,            
        ]);
    }
    public function changeView($data){
        $this->reset(['typeIdentification', 'identification', 'dv', 'firstName', 'secondName', 
        'firtsLastName', 'secondLastName', 'razonSocial', 'address', 'city','typeTercero', 
        'phone', 'rLegal', 'acompañante','rFuente', 'rIVA', 'rIndustraComercio' , 'viewButton']);
        $this->view = $data;

    }
    public function edit($id)
    {
            
            $tercero = Terceros::find($id);  
            $this->terceroId = $tercero->id;    
            $this->typeIdentification = $tercero->typeIdentification;
            $this->identification = $tercero->identification;
            $this->dv = $tercero->dv;
            $this->firstName = $tercero->firstName;
            $this->secondName = $tercero->secondName;
            $this->firtsLastName = $tercero->firtsLastName;
            $this->secondLastName = $tercero->secondLastName;
            $this->razonSocial = $tercero->razonSocial;
            $this->address = $tercero->address;
            $this->city = $tercero->city;
            $this->typeTercero = $tercero->typeTercero;
            $this->phone = $tercero->phone;
            $this->rLegal = $tercero->rLegal;
            $this->acompañante = $tercero->acompañante;
            $this->rFuente = $tercero->rFuente;
            $this->rIVA = $tercero->rIVA;
            $this->rIndustraComercio = $tercero->rIndustraComercio;
            

            $this->view = "store";
            $this->viewButton = "edit";

    }
    public function cancel()
    {
        
        $this->reset(['typeIdentification', 'identification', 'dv', 'firstName', 'secondName', 
        'firtsLastName', 'secondLastName', 'razonSocial', 'address', 'city','typeTercero', 
        'phone', 'rLegal', 'acompañante','rFuente', 'rIVA', 'rIndustraComercio' ,'viewButton']);
        $this->view = 'show';  
    }
    public function confirmed()
    {
        // Example code inside confirmed callback
        Terceros::destroy($this->terceroId);
        $this->alert(
            'success',
            'Usuario eliminado correctamente'
        );
    }

    public function cancelled()
    {       
        $this->alert('info', 'Cancelado');
    }
    public function destroy($id)
    {   
        $this->terceroId = $id;
        $this->confirm('¿Estas seguro?', [
            'toast' => false,
            'position' => 'center',
            'showConfirmButton' => true,
            'confirmButtonText' =>  'Eliminar', 
            'cancelButtonText' => 'Cancelar',
            'onConfirmed' => 'confirmed',
            'onCancelled' => 'cancelled'
        ]);
        
    
        
    }
    public function update(){
        $messages = [
            'typeIdentification.required' => 'El campo tipo de indentificación es obligatorio',
            'identification.required' => 'El campo indentificación es obligatorio',
            'identification.integer' => 'El campo indentificación debe ser un número',
            'dv.integer' => 'El campo d.v debe ser un número',           
            'address.required' => 'El campo dirección es obligatorio',
            'city.required' => 'El campo ciudad es obligatorio',
            'typeTercero.required' => 'El campo tipo tercero es obligatorio',
            'phone.required' => 'El campo teléfono tercero es obligatorio',
            'phone.integer' => 'El campo teléfono deber ser un número',            
        ];
        $this->validate([
            'typeIdentification' => 'required',
            'identification' => 'integer',           
            'address' => 'required',
            'city' => 'required',
            'typeTercero' => 'required',
            'phone' => 'required|integer',
        ], $messages);
        $tercero = Terceros::find($this->terceroId);
        if(empty($this->rFuente)) {
            $this->rFuente = 0;
        };
        if(empty($this->rIVA)) {
            $this->rIVA = 0;
        };
        if(empty($this->rIndustraComercio)) {
            $this->rIndustraComercio = 0;
        }; 
        $tercero->update([            
            'typeIdentification' => $this->typeIdentification,
            'identification' => $this->identification,
            'dv' => $this->dv,
            'firstName' => $this->firstName,
            'secondName' => $this->secondName,
            'firtsLastName' => $this->firtsLastName,
            'secondLastName' => $this->secondLastName,
            'razonSocial' =>  $this->razonSocial,
            'address' => $this->address,   
            'city' => $this->city,   
            'typeTercero' => $this->typeTercero,   
            'phone' => $this->phone,   
            'rLegal' => $this->rLegal,   
            'acompañante' => $this->acompañante,           
            'rFuente' => $this->rFuente,
            'rIVA' => $this->rIVA,
            'rIndustraComercio' => $this->rIndustraComercio,  
        ]);
        $this->reset(['typeIdentification', 'identification', 'dv', 'firstName', 'secondName', 
        'firtsLastName', 'secondLastName', 'razonSocial', 'address', 'city','typeTercero', 
        'phone', 'rLegal', 'acompañante','rFuente', 'rIVA', 'rIndustraComercio' ]);
        $this->view = 'show';
        $this->alert('success', 'Usuario actualizado correctamente!', [
            'position' =>  'top-end', 
            'timer' =>  '4000', 
            'toast' =>  true, 
            'text' =>  '', 
            'confirmButtonText' =>  'Ok', 
            'cancelButtonText' =>  'Cancel', 
            'showCancelButton' =>  false, 
            'showConfirmButton' =>  false, 
        ]);
    }    
    public function store()
    {        
        $messages = [
            'typeIdentification.required' => 'El campo tipo de indentificación es obligatorio',
            'identification.required' => 'El campo indentificación es obligatorio',
            'identification.integer' => 'El campo indentificación debe ser un número',
            'address.required' => 'El campo dirección es obligatorio',
            'city.required' => 'El campo ciudad es obligatorio',
            'typeTercero.required' => 'El campo tipo tercero es obligatorio',
            'phone.required' => 'El campo teléfono tercero es obligatorio',
            'phone.integer' => 'El campo teléfono deber ser un número',
            
        ];
        $this->validate([
            'typeIdentification' => 'required',
            'identification' => 'integer',           
            'address' => 'required',
            'city' => 'required',
            'typeTercero' => 'required',
            'phone' => 'required|integer',
            'typeTercero' => 'required',
        ], $messages);
        if(empty($this->rFuente)) {
            $this->rFuente = 0;
        };
        if(empty($this->rIVA)) {
            $this->rIVA = 0;
        };
        if(empty($this->rIndustraComercio)) {
            $this->rIndustraComercio = 0;
        };  
        Terceros::create([
            'typeIdentification' => $this->typeIdentification,
            'identification' => $this->identification,
            'dv' => $this->dv,
            'firstName' => $this->firstName,
            'secondName' => $this->secondName,
            'firtsLastName' => $this->firtsLastName,
            'secondLastName' => $this->secondLastName,
            'razonSocial' =>  $this->razonSocial,
            'address' => $this->address,   
            'city' => $this->city,   
            'typeTercero' => $this->typeTercero,   
            'phone' => $this->phone,   
            'rLegal' => $this->rLegal,   
            'acompañante' => $this->acompañante,           
            'rFuente' => $this->rFuente,
            'rIVA' => $this->rIVA,
            'rIndustraComercio' => $this->rIndustraComercio,            
        ]);
        $this->reset(['typeIdentification', 'identification', 'dv', 'firstName', 'secondName', 
        'firtsLastName', 'secondLastName', 'razonSocial', 'address', 'city','typeTercero', 
        'phone', 'rLegal', 'acompañante','rFuente', 'rIVA', 'rIndustraComercio' ]);
        $this->view = "show";
        $this->alert('success', 'Tercero creado correctamente!', [
            'position' =>  'top-end', 
            'timer' =>  '4000', 
            'toast' =>  true, 
            'text' =>  '', 
            'confirmButtonText' =>  'Ok', 
            'cancelButtonText' =>  'Cancel', 
            'showCancelButton' =>  false, 
            'showConfirmButton' =>  false, 
        ]);
        
    }
    
}
