<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AgendarCitas extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'id_tercero_paciente', 'id_tercero_medico', 'fecha_consulta','observacion'      
    ];

    public function paciente(){
        return $this->hasMany('App\Models\Terceros');
    }
}