<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AgendarCitas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agendar_citas', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('id_tercero_paciente');
            $table->unsignedInteger('id_tercero_medico');
            $table->foreign('id_tercero_paciente')->references('id')->on('terceros');
            $table->foreign('id_tercero_medico')->references('id')->on('terceros');
            $table->dateTime('fecha_consulta');
            $table->string('observacion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agendar_citas');
    }
}
