<div class="bg-white grid sm:grid-cols-2 lg:grid-cols-4 overflow-hidden">
    <form class="px-5 pt-5 col-span-1" wire:submit.prevent="create">
        <div class="px-4 py-5 bg-white sm:p-6">
            <div class="grid grid-cols-1">
                <div class="col-span-12 mb-5">
                    <label for="nombre_paciente" class="block text-sm font-medium text-gray-700">Nombre del paciente</label>
                    <select wire:model.lazy="paciente" id="id_tercero_paciente" name="id_tercero_paciente" autocomplete="" class="selecttype mt-1 block w-full py-2 px-3 border text-gray-600 border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                        <option hidden>Seleccionar...</option>
                        @foreach ($clientes as $cliente)
                        <option value="{{ $cliente->id }}"> {{ $cliente->firstName }} {{ $cliente->id }}</option>
                        @endforeach
                    </select>
                    @error('paciente')
                    <span class="text-sm text-red-600">{{ $message }}</span>
                    @enderror
                </div>

                <div class="col-span-12 mb-5">
                    <label for="nombre_doctor" class="block text-sm font-medium text-gray-700">Nombre del doctor</label>
                    <select wire:model.lazy="doctor" id="id_tercero_medico" name="id_tercero_medico" autocomplete="" class="selecttype mt-1 block w-full py-2 px-3 border text-gray-600 border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                        <option hidden>Seleccionar...</option>
                        @foreach ($medicos as $tercero)
                        <option value="{{ $tercero->id }}"> {{ $tercero->firstName }} {{ $tercero->id }}</option>
                        @endforeach
                    </select>
                    @error('doctor')
                    <span class="text-sm text-red-600">{{ $message }}</span>
                    @enderror
                </div>

                <div class="col-span-12 mb-5">
                    <label for="fecha" class="block text-sm font-medium text-gray-700">Fecha de la Consulta</label>
                    <input wire:model.lazy="datetimes" type="datetime-local" name="datetimes" id="datetimes" autocomplete="off" class="selecttype mt-1 block w-full py-2 px-3 border text-gray-600 border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                    @error('datetimes')
                    <span class="text-sm text-red-600">{{ $message }}</span>
                    @enderror
                </div>

                <div class="col-span-12 mb-5">
                    <label for="" class="block text-sm font-medium text-gray-700">Observación</label>
                    <textarea wire:model.lazy="observacion" type="text" name="observacion" id="observacion" autocomplete="off" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"></textarea>
                    @error('observacion')
                    <span class="text-sm text-red-600">{{ $message }}</span>
                    @enderror
                </div>
                <div class="px-4 py-3 text-right sm:px-6">
                    <button type="submit" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                        <span wire:loading.attr="hidden">{{ __('Save') }} </span>
                        <span wire:loading wire:target="store">Procesando...</span>
                    </button>
                </div>
            </div>
        </div>
    </form>

    <div id="calendar" class="bg-white my-8 mx-10 col-span-3 overflow-hidden"></div>


    @foreach($citas as $cita)
    @endforeach
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var calendarEl = document.getElementById('calendar');
            var calendar = new FullCalendar.Calendar(calendarEl, {
                editable: true,
                headerToolbar: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'dayGridMonth,timeGridWeek,timeGridDay'
                },
                navLinks: true,
                selectable: true,
                selectMirror: true,


                events: [{

                    'title': "{{ $cita->observacion }}",
                    'start': "{{ $cita->fecha_consulta }}"

                }]

            });
            calendar.setOption('locale', 'es');
            calendar.render();
        });
    </script>

</div>