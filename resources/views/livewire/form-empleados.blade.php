<div>
    <div class="grid bg-white  sm:grid-cols-1 lg:grid-cols-2">
        <div>
            @foreach ($empleados as $empleado)
                <p>{{$empleado->cEmpleado}}{{$empleado->name}}{{$empleado->cargo}}</p>               
            @endforeach
        </div>
        <div>
            <div class="mt-5">
                @if ($viewButton == 'store')
                    <form class="px-5" wire:submit.prevent="store">
                    @elseif($viewButton == 'edit')
                        <form class="px-5" wire:submit.prevent="update">
                @endif
                <div class="grid lg:grid-cols-3 md:grid-cols-2 sm:grid-cols-1 gap-1">
                    <div class="mb-5 col-span-3">
                        <label for="cEmpleado" class="block text-sm font-medium text-gray-700">Código del vendedor
                        </label>
                        <input style="text-transform:capitalize" wire:model.lazy="cEmpleado" type="number"
                            name="cEmpleado" id="second_name" autocomplete="off"
                            class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                        @error('cEmpleado')
                            <span class="text-sm text-red-600">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="mb-5 col-span-3">
                        <label for="name" class="block text-sm font-medium text-gray-700">Nombre del empleado
                        </label>
                        <input style="text-transform:capitalize" wire:model.lazy="name" type="text" name="name"
                            id="name" autocomplete="off"
                            class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                        @error('name')
                            <span class="text-sm text-red-600">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="mb-4 col-span-3">
                        <label for="cargo" class="block text-sm font-medium text-gray-700">Cargo del empleado</label>
                        <select wire:model.lazy="cargo" id="cargo" name="cargo" autocomplete="cargo"
                            class="mt-1 block w-full py-2 px-3 border text-gray-600 border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                            <option hidden>Seleccionar...</option>
                            <option value="Vendedor">Vendedor</option>
                            <option value="Mecánico">Mecánico</option>
                            <option value="Tecnico">Tecnico</option>
                            <option value="Técnico">Técnico</option>
                        </select>
                        @error('cargo')
                            <span class="text-sm text-red-600">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                @if ($viewButton == 'store')

                    <div class="px-4 py-3 text-right sm:px-6">

                        <button type="submit"
                            class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                            <span wire:loading.attr="hidden">{{ __('Save') }} </span>
                            <span wire:loading wire:target="store">Procesando...</span>
                        </button>

                    </div>
                @elseif($viewButton == 'edit')
                    <div class="px-4 py-3 text-right sm:px-6">
                        <a role="button" wire:click="cancel"
                            class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-red-600 hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                            <span>Cancelar</span>
                        </a>

                        <button type="submit"
                            class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                            <span wire:loading.attr="hidden">Actualizar</span>
                            <span wire:loading wire:target="update">Procesando...</span>
                        </button>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
