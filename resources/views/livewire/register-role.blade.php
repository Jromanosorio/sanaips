<x-app-layout>
    <div class="bg-white grid grid-cols-2 overflow-hidden shadow-xl sm:rounded-lg">
        <div class="flex flex-col divide-x divide-gray-200">
            <div class=" overflow-x-auto sm:-mx-6 lg:-mx-8">
                <div class="align-middle inline-block min-w-full sm:px-6 lg:px-8">
                    <div class="overflow-hidden border-b border-gray-200 sm:rounded-lg">
                        <table class="min-w-full divide-y divide-gray-200">
                            <thead class="bg-gray-50">
                                <tr>
                                    <th scope="col"
                                        class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                        Nombre
                                    </th>
                                    <th scope="col"
                                        class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                        Role
                                    </th>

                                    <th scope="col" class="relative px-6 py-3">
                                        <span class="sr-only">Edit</span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody class="bg-white divide-y divide-gray-200">

                                {{-- @foreach ($roles as $role) --}}


                                    <tr>
                                        <td class="px-6 py-4 whitespace-nowrap">
                                            <div class="flex items-center">
                                                {{-- <div class="flex-shrink-0 h-10 w-10">
                                                    <img class="h-10 w-10 rounded-full"
                                                        src="{{ $user->profile_photo_url }}"
                                                        alt="{{ $user->name }}">
                                                </div> --}}
                                                <div class="ml-4">
                                                    <div class="text-sm font-medium text-gray-900">
                                                        {{-- {{ $user->name }} --}}
                                                    </div>
                                                    <div class="text-sm text-gray-500">
                                                        {{-- {{ $user->email }} --}}
                                                    </div>
                                                </div>
                                            </div>
                                        </td>

                                        <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                            {{-- {{ $user->role }} --}}
                                        </td>
                                        {{-- <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                                            <a wire:click="update({{ $user->id }})" href="#"
                                                class="text-indigo-600 hover:text-indigo-900"><i
                                                    class="fas fa-edit"></i></a>
                                            <a wire:click="destroy({{ $user->id }})" href="#"
                                                class="text-red-600 hover:text-red-900"><i
                                                    class="fas fa-trash"></i></a>
                                        </td> --}}
                                    </tr>
                                {{-- @endforeach --}}


                                <!-- More rows... -->
                            </tbody>
                        </table>
                            {{-- {{ $users->links() }} --}}
                        
                    </div>
                </div>
            </div>
        </div>
        <form class="px-5 pt-5" wire:submit.prevent="store">
            <div class="overflow-hidden">
                <div class="px-4 py-5 bg-white sm:p-6">
                    <div class="grid grid-cols-12">
                        <div class="col-span-12 sm:col-span-12 mb-5">
                            <label for="first_name" class="block text-sm font-medium text-gray-700">Nombre
                                del role</label>
                            <input style="text-transform:capitalize" wire:model.lazy="rolename" type="text"
                                name="first_name" id="first_name" autocomplete="off"
                                class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                            @error('name')
                                <span class="text-sm text-red-600">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="col-span-12 sm:col-span-3 mb-5">
                            <label for="street_address"
                                class="block text-sm font-medium text-gray-700">Descripción</label>
                            <textarea wire:model.lazy="description" type="password" name="street_address"
                                id="street_address" autocomplete="off"
                                class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"></textarea>
                            @error('description')
                                <span class="text-sm text-red-600">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="px-4 py-3 text-right sm:px-6">
                    <button type="submit"
                        class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                        <span wire:loading.attr="hidden">{{ __('Save') }} </span>
                        <span wire:loading wire:target="store">Procesando...</span>
                    </button>


                </div>
            </div>
        </form>
    </div>
</x-app-layout>
