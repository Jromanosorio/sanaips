<?php

use Illuminate\Support\Facades\Route;
use App\Http\Livewire\RegistrarIngreso;
use App\Http\Livewire\RegistrarTercero;
use App\Http\Livewire\RegisterUser;
use App\Http\Livewire\RegistrarFactura;
use App\Http\Livewire\RegistrarEmpleados;
use App\Http\Livewire\FormCitas;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your aRegistrarFacturapplication. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
// Route::middleware(['auth:sanctum', 'verified'])->get('/register-user', function () {
//     return view('livewire.register-user');
// })->name('register-user');
Route::middleware(['auth:sanctum', 'verified'])->get('/register-role', function () {
    return view('livewire.register-role');
})->name('register-role');
// Route::middleware(['auth:sanctum', 'verified'])->get('/registrar-tercero', function () {
//     return view('livewire.registrar-tercero');
// })->name('registrar-tercero');

Route::middleware(['auth:sanctum', 'verified'])->get('/register-user', RegisterUser::class)->name('register-user');
Route::middleware(['auth:sanctum', 'verified'])->get('/agendar-citas', FormCitas::class)->name('agendar-citas');
Route::middleware(['auth:sanctum', 'verified'])->get('/registrar-ingreso', RegistrarIngreso::class)->name('registrar-ingreso');
Route::middleware(['auth:sanctum', 'verified'])->get('/registrar-tercero', RegistrarTercero::class)->name('registrar-tercero');
Route::middleware(['auth:sanctum', 'verified'])->get('/registrar-factura', RegistrarFactura::class)->name('registrar-factura');
Route::middleware(['auth:sanctum', 'verified'])->get('/registrar-empleado', RegistrarEmpleados::class)->name('registrar-empleado');
